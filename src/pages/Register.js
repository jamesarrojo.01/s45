import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';



export default function Register() {

	const { user } = useContext(UserContext);
	
	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [gender, setGender] = useState('Select');
	const [mobileNo, setMobileNo] = useState('');

	// State to determine whether submit button is enabled or not for conditional rendering

	const [isActive, setIsActive] = useState(true)

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && gender !== 'Select' && mobileNo !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2, gender, mobileNo])

	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();

		// // Clear input fields
		// setEmail('');
		// setPassword1('');
		// setPassword2('');

		// Swal.fire({
		// 	title: "Yaaaaaaaaay!",
		// 	icon: "success",
		// 	text: "You have successfully registered"
		// })

		// assignment

		fetch('https://b165-shared-api.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				gender: gender,
				mobileNo: mobileNo
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data) {
				Swal.fire({
					title: 'Account Created!!',
					icon: 'success',
					text: "Welcome Aboard!"
				})

				// GONNA ADD CLEAR FIELDS BELOW....
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: "account not created!!"
				})
			}

		})
	}


	return(

		// Conditional rendering

		(user.accessToken !== null) ?

		<Navigate to="/" />

		:

		<Form onSubmit={(e => registerUser(e))}>
			<h1>Register</h1>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Your First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Your Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text>
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Gender</Form.Label>
				<Form.Select onChange={e => setGender(e.target.value)}>
					<option>Select</option>
					<option value="male">Male</option>
					<option value="female">Female</option>
				</Form.Select>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile No.</Form.Label>
				<Form.Control
					type="tel"
					placeholder="09123456789"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>
			{isActive ?

				<Button variant="primary" type="submit" className="mt-3">Submit</Button>
				:

				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>

			}




		</Form>








		)
}