const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni labore, natus reiciendis nisi doloribus possimus in quod pariatur, aspernatur ratione obcaecati adipisci. Officiis eum laborum deserunt asperiores possimus magnam, dolorum?",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni labore, natus reiciendis nisi doloribus possimus in quod pariatur, aspernatur ratione obcaecati adipisci. Officiis eum laborum deserunt asperiores possimus magnam, dolorum?",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni labore, natus reiciendis nisi doloribus possimus in quod pariatur, aspernatur ratione obcaecati adipisci. Officiis eum laborum deserunt asperiores possimus magnam, dolorum?",
		price: 55000,
		onOffer: true
	}




]
export default coursesData;